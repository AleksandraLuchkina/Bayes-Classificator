from collections import Counter

#function to save objects without '?'
def Clean_Data(Name_of_File):
    file = open(Name_of_File)
    Dataset = list()
    for line in file:
        indicator = 0
        for symbol in line:
            if symbol == '?':
                indicator = 1
        if indicator == 0:
            Dataset.append(line.split(','))
    file.close()
    return Dataset

#function to calculate probability of the Feature in the Class
def Probability_of_feature(Feature, Clas, Data):
    Values_of_feature = list()
    summa = 0
    for obj in Data:
        if obj[-1] == Clas:
            Values_of_feature.append(obj[Feature])
            summa += 1
    Prob_feature = Counter(Values_of_feature)
    for key in Prob_feature:
        Prob_feature[key] /= summa
    return Prob_feature

#Read Data set and save object without '?'
Name_of_File = input('Enter name of file with training data\n')
Data = Clean_Data(Name_of_File)
Name_of_File = input('Enter name of file with test Data\n')
ProvData = Clean_Data(Name_of_File)

#Calculate Probability of Classes
Classes = list()
for obj in Data:
    Classes.append(obj[-1])
Probability_class = dict()
Probability_class = Counter(Classes)
for key in Probability_class:
    Probability_class[key] /= len(Data)

#Calculate Probability of Features in Every Class
Probability_feature = dict()
for clas in set(Classes):
    Probability_feature[clas] = dict()
    for Feature in range(len(Data[0])):
        Probability_feature[clas][Feature] = dict()
        Probability_feature[clas][Feature] = Probability_of_feature(Feature, clas, Data)

#Naive Bayes algorithm implement to Prove Data
True_classes = list()
Result_classes = list()
for obj in ProvData:
    maximum_probability = 0
    True_classes.append(obj[-1])
    obj[-1] == -1
    for clas in set(Classes):
        probability = Probability_class[clas]
        for Feature in range(len(ProvData[0]) - 1):
            probability *= Probability_feature[clas][Feature][obj[Feature]]
        if maximum_probability <= probability:
            maximum_probability = probability
            obj[-1] = clas
    Result_classes.append(obj[-1])

#Calculate accuracy of the algorithm
accuracy = 0
for index in range(len(True_classes)):
    if True_classes[index] == Result_classes[index]:
        accuracy += 1
accuracy /= len(True_classes)
print(accuracy)
